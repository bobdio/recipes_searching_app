class RecipeFinder {
  constructor(container) {
    this.recipesContainer = document.getElementById(container)
  }

  // listening key up event to perform searh by entered text
  init() {
    document.getElementById('search').oninput = (event) => {
      let text = event.target.value
      if (text.length) {
        this._searchRecipe(text).then(
          response => this._handleSuccess(response.results),
          err => this._handleError(err)
        )
      } else {
        this.recipesContainer.innerHTML = ''
      }
    }
  }

  // show recipes to the user
  _handleSuccess(results) {
    let content;
    
    if (results.length) {
      if (document.getElementById('search').value) {
        content = results.map((recipe) => {
          return this._recipeTemplate(recipe)
        }).join('');
      } else {
        content = ''
      }

    } else {
      content = `<div class="alert alert-warning" role="alert">
                  There was no recipe found by entered text. Try to enter another text
                </div>`
    }

    this.recipesContainer.innerHTML = content;
  }

  _handleError(err) {
    console.error(err)
    this.recipesContainer.innerHTML = `<div class="alert alert-danger" role="alert">
                                        There was an error during recipes searching. Try again later
                                      </div>`
  }

  _recipeTemplate(recipe) {
    return `<div class="list-group-item">
              <h3>${recipe.title}</h3>
              <div class="row">
                <div class="col-lg-10 col-xs-12">
                  <p>
                    <strong>ingredients: </strong>${recipe.ingredients}
                    <a href="${recipe.href}" target="_blank">more...<a>
                  </p>
                </div>
                <div class="col-lg-2 col-xs-12 text-center">
                    <img src="${recipe.thumbnail}" />
                </div>
              </div>
            </div>`
  }

  // a method to perform api call
  // returns a Promise object
  _searchRecipe(text) {
    return new Promise((resolves, rejects) => {
      const api = `api/search_recipe.json?text=${text}`
      const request = new XMLHttpRequest()
      request.open('GET', api)
      request.onload = () =>
           (request.status === 200) ?
            resolves(JSON.parse(request.response)) :
            reject(Error(request.statusText))
      request.onerror = (err) => rejects(err)
      request.send()
    })
  }
}

document.addEventListener('DOMContentLoaded', () => {
  new RecipeFinder('recipes-container').init()
}, false);
