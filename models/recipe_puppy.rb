require 'active_support/core_ext/object/to_query'

# a class to perform recipes searching
class RecipePuppy
  RECIPE_PUPPY_URL = 'http://www.recipepuppy.com/'

  def initialize
    establish_connection
  end

  # Public: search for recipes by passet text
  #
  # text - a string to perform search, required.
  #
  # Examples
  #
  #   :RecipePuppy.new.search('meat')
  #
  # Returns an array with recipes.
  def search(text, page: 1)
    results = get 'api', q: text, p: page * 2 - 1, format: :json

    # since RecipePuppy API returns only 10 recipes per request
    # we need to perform one more request to get 20 recipes
    if results.size == 10 # if less then 10 we don't need request one more time
      next_page = get 'api', q: text, p: page * 2, format: :json
      results.concat next_page
    end

    results
  end

  # Public: make Get request to pased path with params
  #
  # path - a string, required.
  # params - a hash to filter search, optional
  #
  # Examples
  #
  #   :RecipePuppy.new.get('api', page: 1)
  #
  # Returns faraday response object
  def get(path, params = {})
    response = @connection.get "api/?#{params.to_query}"
    response.body.fetch('results', [])
  end

  private

  def establish_connection
    @connection = Faraday.new RECIPE_PUPPY_URL do |conn|
      conn.use Faraday::Response::RaiseError
      conn.use FaradayMiddleware::ParseJson, content_type: 'text/javascript'
      conn.use Faraday::Adapter::NetHttp
    end
  end
end
