require './app.rb'
require 'spec_helper'

RSpec.describe RecipeFinder do
  let(:app) { RecipeFinder.new }

  context 'GET index page' do
    it 'loads main page' do
      get '/'
      expect(last_response.status).to eq 200
    end
  end

  context 'GET search recipes' do
    let(:model) { RecipePuppy }

    context 'when there are some recipes' do
      let(:recipes) do
        [
          {
            title: 'All Purpose Ground Meat Mix',
            href: 'http://www.recipezaar.com/All-Purpose-Ground-Meat-Mix-31008',
            ingredients: 'celery, garlic, green pepper, ground beef, onions, black pepper, salt',
            thumbnail: 'http://img.recipepuppy.com/45141.jpg'
          },
          {
            title: 'Moms Meat Loaf Recipe',
            href: 'http://www.grouprecipes.com/76908/moms-meat-loaf.html',
            ingredients: 'green beans, barley, eggs, ground beef, seasoning, bread crumbs',
            thumbnail: 'http://img.recipepuppy.com/334313.jpg'
          }
        ]
      end

      it 'returns list of recipes' do
        allow_any_instance_of(model).to receive(:search).and_return(recipes)
        get 'api/search_recipe.json', params: { text: 'meat' }
        expect(JSON.parse(last_response.body)['results'].size).to eq(2)
      end
    end

    context 'when there is no recipe' do
      it 'returns empty list' do
        allow_any_instance_of(model).to receive(:search).and_return([])
        get 'api/search_recipe.json', params: { text: 'AAAAAAA' }
        expect(JSON.parse(last_response.body)['results'].size).to eq(0)
      end
    end
  end
end
