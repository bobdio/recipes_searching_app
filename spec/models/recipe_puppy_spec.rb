require './models/recipe_puppy'
require 'spec_helper'

RSpec.describe RecipePuppy do
  subject { RecipePuppy }

  describe '#search' do
    context 'when no recipes found' do
      it 'returns empty array' do
        allow_any_instance_of(subject).to receive(:get).and_return([])
        expect(subject.new.search('test')).to eq([])
      end
    end

    context 'when there are recipes' do
      context 'and there are less then 10 recipes' do
        let(:recipes_count) { 7 }
        let(:recipes) do
          [
            {
              title: 'Moms Meat Loaf Recipe',
              href: 'http://www.grouprecipes.com/76908/moms-meat-loaf.html',
              ingredients: 'green beans, barley, eggs, ground beef, seasoning',
              thumbnail: 'http://img.recipepuppy.com/334313.jpg'
            }
          ] * recipes_count
        end

        it 'performs only one get request' do
          allow_any_instance_of(subject).to receive(:get).and_return(recipes)

          finder = subject.new
          results = finder.search('meat')
          expect(finder).to have_received(:get).once
          expect(results.size).to eq(recipes_count)
        end
      end
      context 'and there are 10 recipes' do
        let(:recipes_count) { 10 }
        let(:recipes) do
          [
            {
              title: 'Moms Meat Loaf Recipe',
              href: 'http://www.grouprecipes.com/76908/moms-meat-loaf.html',
              ingredients: 'green beans, barley, eggs, ground beef, seasoning',
              thumbnail: 'http://img.recipepuppy.com/334313.jpg'
            }
          ] * recipes_count
        end

        it 'performs two get requests' do
          allow_any_instance_of(subject).to receive(:get).and_return(recipes)

          finder = subject.new
          results = finder.search('meat')

          expect(finder).to have_received(:get).twice
          expect(results.size).to eq(recipes_count * 2)
        end
      end
    end
  end
end
