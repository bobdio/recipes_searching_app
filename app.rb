require './models/recipe_puppy'
require 'sinatra'

# main class
class RecipeFinder < Sinatra::Base
  set :public_folder, File.dirname(__FILE__) + '/assets'

  # main page
  get '/' do
    erb :search
  end

  # simple api endpoint to search recipes
  get '/api/search_recipe.json' do
    content_type :json
    recipes = RecipePuppy.new.search(params[:text])
    { results: recipes }.to_json
  end
end
