## A simple sinatra application to search recipes

to run:

  * install needed gems by `bundle install`
  * go to the project folder in terminal and run `rackup` command. App will be available on http://localhost:9292/
  * Heroku link https://recipe-finder-puppy.herokuapp.com/
  * you can run tests by `bundle exec rspec spec` 
